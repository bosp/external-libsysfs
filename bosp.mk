
ifdef CONFIG_EXTERNAL_LIBSYSFS

# Targets provided by this project
.PHONY: libsysfs clean_libsysfs

# Add this to the "external" target
external: libsysfs
clean_external: clean_libsysfs
distclean_external: distclean_libsysfs

MODULE_DIR_LIBSYSFS=$(BASE_DIR)/external/required/sysfsutils
BUILD_DIR_LIBSYSFS=$(MODULE_DIR_LIBSYSFS)/build/

libsysfs: setup $(BUILD_DIR_LIBSYSFS)/Makefile
	@echo
	@echo "==== Building/Installing SysFS Library v2.1.0 ===="
	@echo
	@cd $(BUILD_DIR_LIBSYSFS) && \
		make -j$(CPUS) install
	@echo

$(MODULE_DIR_LIBSYSFS)/configure: 
	@echo
	@echo "==== Auto-Configure SysFS Library v2.1.0 ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@cd $(MODULE_DIR_LIBSYSFS) && autoreconf -fi

$(BUILD_DIR_LIBSYSFS)/Makefile: $(MODULE_DIR_LIBSYSFS)/configure
	@echo
	@echo "==== Configuring SysFS Library v2.1.0 ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	[ -d $(BUILD_DIR_LIBSYSFS) ] || \
		mkdir -p $(BUILD_DIR_LIBSYSFS)
	@cd $(BUILD_DIR_LIBSYSFS) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS) -I$(MODULE_DIR_LIBSYSFS)/include" \
		$(MODULE_DIR_LIBSYSFS)/configure \
		--host=$(PLATFORM_TARGET) \
		--prefix=$(BOSP_SYSROOT)/usr \
		--disable-klibc

clean_libsysfs:
	@echo
	@echo "==== Cleanup SysFS Lbrary v2.1.0 ===="
	[ -d $(BUILD_DIR_LIBSYSFS) ] && cd $(BUILD_DIR_LIBSYSFS) && \
		make clean || echo "No build directory"

distclean_libsysfs:
	@echo
	@echo "==== Dist-Cleanup SysFS Lbrary v2.1.0 ===="
	@[ -d $(BUILD_DIR_LIBSYSFS) ] && cd $(BUILD_DIR_LIBSYSFS) && \
		make distclean || echo "No build directory"


else # CONFIG_EXTERNAL_LIBSYSFS

libsysfs:
	$(warning $(MODULE_DIR_LIBSYSFS) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_LIBSYSFS

